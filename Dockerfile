FROM ruby:alpine

# Setup the user
RUN adduser -S -D -h /quote-bot quote-bot
RUN chown -R quote-bot /quote-bot
RUN chmod -R 777 /quote-bot
USER quote-bot

# Set the workdir and copy the code in
WORKDIR /quote-bot
COPY . .

# Install dependencies
RUN rake deps

CMD rake run
