#!/usr/bin/env ruby

# This is a simple standalone script that
# just causes a bot to leave all the servers
# that it is part of.

require "dotenv/load"
require "discordrb"

bot = Discordrb::Bot.new(token: ENV["TOKEN"])

bot.ready do |event|
  bot.servers.each_value do |server|
    puts server.to_s
    server.leave
  end
  bot.stop
end

bot.run
