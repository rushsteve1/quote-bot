# Quote-Bot

A Discord bot written in [Ruby](https://www.ruby-lang.org) that stores
and re-quotes people on command.
A good way to be reminded of all the funny things people say.

Written using [Discordrb](https://github.com/meew0/discordrb).

## Getting Started

Ensure that you have Ruby, Rake, and Bundler installed.

Install the dependencies with
```
rake deps
```

Then run the bot with:
```
rake run
```

