#!/usr/bin/env ruby

require "logger"
require "dotenv/load"
require "discordrb"
require "./store.rb"
require "set"

# Set the title of this process
Process.setproctitle("quote-bot")
$0="quote-bot"

if ENV["STORE_PATH"].nil?
  $stderr.puts "`STORE_PATH` environment variable is not set. Please set it in .env"
  exit 1
end

if ENV["TOKEN"].nil?
  $stderr.puts "`TOKEN` environment variable is not set. Please set it in .env"
  exit 1
end

if ENV["OWNER_ID"].nil?
  $stderr.puts "`OWNER_ID` environment variable is not set. Please set it in .env, without it the `update` command is not available."
end

# Setup the bot with the token and prefix
bot = Discordrb::Commands::CommandBot.new(token: ENV["TOKEN"], prefix: ")")

# Print out the invlite link for the bot
puts "Invite Link: https://discordapp.com/oauth2/authorize?&client_id=#{bot.bot_application.id}&scope=bot&permissions=0"

# Setup the logger
LOG_FILE = File.new('quote-bot.log', 'a')
logger = Logger.new(LOG_FILE, 'weekly')
logger.level = :debug

# Move this process into the background without changing directories
# Only backgrounds if given the 'bg' argument
if ARGV.include?  "bg"
  # Redirect stdout and stderr
  $stdout = LOG_FILE
  $stderr = LOG_FILE

   Process.daemon(nochdir=true)
end

# Get a member from a user mention and an event.
def get_member(event, mention)
    if event
        u = event.bot.parse_mention(mention, event.server)
        if u.nil?
            return event.server.member(event.author.id)
        else
            return event.server.member(u.id)
        end
    end
    return nil
end

# When the bot is ready
bot.ready do |event|
  # Set the "game" to the help command
  bot.game = ")help for help"
  logger.info "Bot Started"
end

store = Store.new(ENV["STORE_PATH"])

# The help command
# PMs the user the help text
bot.command :help do |event|
  event.user.pm.send_embed do |embed|
    embed.title = "Quote-Bot Help"
    embed.description = "
Rushsteve1's wonderfully bad Quote-Bot!
https://git.rushsteve1.us/rushsteve1/quote-bot

`)help`: sends this help text
`)add @user quote text`: adds a quote for @user with the given text
`)list @user`: list all the quotes by @user
`)quote @user`: replay a quote by @user
`)remove @user #` removes the numbered quote from @user. Only available to the user or admins.

Quotes Database can be downloaded from: https://rushsteve1.us/store.csv
"
  end
end

# The add command
# Parses the text from the given command and calls quote_add
bot.command :add do |event, mention, *text|
  if bot.parse_mention(mention).nil?
    text.unshift(mention)
  end
  user = get_member(event, mention)
  if event.server and user
    store.quote_add(event.server.id, user.id, text.join(" "))
    "Added quote for " + (user.nick or user.username)
  else
    "Cannot add a quote from this channel"
  end
end

# The remove command
# Checks that the user has the proper permissions
# and then calls quote_remove
bot.command :remove do |event, mention, ind|
  user = get_member(event, mention)
  if event.server and user
    if user == event.author or event.author.permission?(:administrator)
      # Minus one to account for zero-index
      store.quote_remove(event.server.id, user.id, ind.to_i - 1)
      "Removed quote number " + ind.to_s + " for " + (user.nick or user.username)
    else
      "You do not have permission to remove that quote"
    end
  else
    "Cannot remove a quote from this channel"
  end
end

# The list command
# Gets the quotes by a user and puts them into a nice pretty embed list
bot.command :list do |event, mention|
  user = get_member(event, mention)
  if event.server and user
    q = store.quotes_user(event.server.id, user.id)
    if q.empty?
      "User has no quotes"
    else
      embed ||= Discordrb::Webhooks::Embed.new
      embed.title = "Quotes by " + user.username
      h = []
      len = 0
      q.each_with_index do |row, i|
        len += row["text"].length

        # If the quote list is too long
        if len > 1000
          # Send the current list
          embed.fields = h
          event.send_embed("", embed)

          # Reset variables
          len = 0
          h = []
          embed ||= Discordrb::Webhooks::Embed.new
          embed.title = "Quotes by " + user.username
        end

        # format the date nicely
        pretty_date = DateTime.parse(row["datetime"]).strftime("%a, %b %e '%y at %l:%M%P")
        h.push({ "name" => "\##{i + 1} from #{pretty_date}", "value" => row["text"], "inline" => false })
      end
          embed.fields = h
      event.send_embed("", embed)
    end
  else
    "Cannot list quotes from this server"
  end
end

# The quote command
# Replays a random quote by a user
bot.command :quote do |event, mention, num|
  user = get_member(event, mention)
  if event.server and user
    q = store.quotes_user(event.server.id, user.id).sample
    num = num.to_i

    if num > 0
      event.send_embed do |embed|
        embed.author = { "name" => user.username, "url" => nil, "icon_url" => user.avatar_url, "proxy_icon_url" => nil }
        embed.description = store.quotes_user(event.server.id, user.id)[num]["text"]
      end
    elsif q
      event.send_embed do |embed|
        embed.author = { "name" => user.username, "url" => nil, "icon_url" => user.avatar_url, "proxy_icon_url" => nil }
        embed.description = q["text"]
      end
    else
      "User has no quotes"
    end
  end
end

# Tells the bot to update itself and restart.
# This is only available to the user whose
# id is set in the OWNER_ID env variable.
bot.command :update do |event| 
  if ENV['OWNER_ID']
    if ENV['OWNER_ID'].to_s == event.author.id.to_s
      # Pull the new code and make sure it worked
      event.send_message "Updating..."
      # Run the pull
      `git pull`
      if $?.success?
        # Runs the bot again when it exits
        event.send_message "Update succeeded, restarting..."
        logger.warn "Bot updated from git. Restarting..."
        # We are still inside the bundle so we can just use ruby
        at_exit { Process.spawn("ruby main.rb") }
        exit
      else
        logger.error "Failed to pull from git"
        "Pull failed"
      end
    else
      logger.warn "User #{event.author.id} tried to update"
      "You do not have permission to run this command"
    end
  else
    "Command not available"
  end
end

# Since the bot will be running as a daemon
# it can be hard to stop.
# This command stops the server at the owner's request.
bot.command :stop do |event, user| 
  if ENV['OWNER_ID']
    if ENV['OWNER_ID'].to_s == event.author.id.to_s
      event.send_message "Goodbye!"
      logger.info "Bot stopped by owner"
      exit
    else
      logger.warn "User #{event.author.id} tried to stop"
      "You do not have permission to run this command"
    end
  else
    "Command not available"
  end
end

$user_set = Set.new

def id_to_set(id)
  if $user_set.include? id
    return false
  else
    $user_set.add(id)
    Thread.new do
      sleep(30)
      $user_set.delete(id)
    end
    return true
  end
end

# When a user comes online
# put a random quote by them into the default channel
# and then delete it 30 seconds later
=begin
bot.presence(status: "online") do |event|
  if event.user
    bot.servers.each_value do |server|
      q = store.quotes_user(server.id, event.user.id).sample
      if q and id_to_set(event.user.id)
        embed ||= Discordrb::Webhooks::Embed.new
        embed.author = { "name" => event.user.username, "url" => nil, "icon_url" => event.user.avatar_url, "proxy_icon_url" => nil }
        embed.description = q["text"]
        event.server.default_channel.send_temporary_message(nil, 30.0, false, embed)
      end
    end
  end
end
=end

bot.run
