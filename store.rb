#!/usr/bin/env ruby

require "csv"

# The class representing the CSV store
#
# The CSV file is ordered as:
# server,    user,    datetime,     text
# server_id, user_id, iso_date_time "quote text"
class Store

  # Headers in the CSV file, written by initialize
  HEADERS = ["server", "user", "datetime", "text"].freeze

  # Setup the class and create the store file if necessay
  def initialize(path)
    @store_path = path
    if not File.file?(@store_path)
      File.open(@store_path, "w") do |f|
        f << HEADERS.join(",")
      end
    end
  end

  # Quotes by all users on a server
  def quotes_server(id)
    read_store do |store|
      store.select { |row| row["server"] == id }
    end
  end

  # Quotes by a user on a server
  def quotes_user(server, user)
    read_store do |store|
      # Performed as a seperate filter than quotes_server for speed
      store.select do |row|
        row["user"].to_s == user.to_s and row["server"].to_s == server.to_s
      end
    end
  end

  # Add a quote by a user on a server
  def quote_add(server, user, quote)
    append_store do |store|
      store << [server, user, Time.new, quote]
    end
  end

  # Remove a quote by a user on a server by it's index
  def quote_remove(server, user, ind)
    q = quotes_user(server, user)[ind]
    if q
      safe_edit do |out_file, line|
        out_file.puts line unless line == q.to_s
      end
    end
  end

  private # The following methods are private

  # Helper wrapper for reading from the store file
  def read_store
    store = CSV.open(@store_path, "r", { :headers => true })
    yield(store)
  end

  # Helper wrapper for writing to the store file
  def append_store
    store = CSV.open(@store_path, "a", { :headers => true })
    yield(store)
  end

  # An alternative to append_store that performs an edit
  # on a seperate file then swaps it for the store file
  def safe_edit
    out_path = ENV["STORE_PATH"] + ".tmp"
    File.open(out_path, "w") do |out_file|
      File.foreach(@store_path) do |line|
        yield(out_file, line)
      end
    end
    FileUtils.mv(out_path, @store_path)
  end
end
